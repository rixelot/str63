# STR64 implementation

Str64 is a way to encode strings composed of ASCII letters, and URI-safe 
punctuation, typically tokens, identifiers or simple words, within a 64 bits value, 
fitting in the registers of 64-bit CPU architectures.

## Definition

The encoding is defined in STR64.md

## Reference implementation

The /rust subdirectory contains a reference encoder/decoder written in Rust.

## Generated implementations

A python script in the /meta subdirectory parses the Rust code and generates other
language implementations:

- /c Standard C (C89 or above)
- /go Go version
- (... more to come)

## No copyright

This work is in the public domain.
