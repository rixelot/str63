""" Convert reference Rust code to golang """
from to_c import ToC
from data_for_tests import EXACT, LOSSY

TARGET_MAGIC = 'TArgET' # used to bubble up variable name in if expressions substitutes

class ToGo(ToC):
	""" Lark parse tree transformer """
	TYPES = {'bool': 'bool',
		'char': 'rune',
		'u8': 'uint8',
		'u64': 'Str64',
		'Str64': 'Str64'}

	def __init__(self):
		self.consts = {}

	# define type
	def typedef_us(self, vals):
		actual_t, = vals
		return "type Str64 uint64".format(actual_t)

	# Generic functions
	def func_bool(self, vals):
		name, param, stmts, ret = vals
		rtype = self.TYPES["bool"]
		self.locals_seen = set()
		return "func {}({}) {} {{ {} return {}; }}".format(name, param, rtype, stmts, ret)
	def func_int(self, vals):
		name, callp, rettyp, body = vals
		self.locals_seen = set()
		return "func {}({}) {} {}".format(name, callp, rettyp, body)
	def func_name(self, vals):
		rawname, = vals
		return rawname
	def func_params(self, vals):
		return ",".join(vals)
	def func_body(self, vals):
		stmts = vals[:-1]
		ret = vals[-1]
		stmts = "\n".join(stmts)
		if TARGET_MAGIC in ret:
			gotype = self.TYPES['u8'] # HACK ad hoc in typing
			ret_stmt = ret.replace(TARGET_MAGIC, 'ret00')
			return "{{\n{}\nvar ret00 {} = 0\n{}\nreturn ret00\n}}\n".format(stmts, gotype, ret_stmt)
		return "{{\n{}\n return {}}}".format(stmts, ret)

	# Hardcoded functions
	def if_opt(self, vals):
		cond, var = vals
		return "{}?-1:{}".format(cond, var)
	def func_string(self, vals):
		name, param, inlinefuncs, stmts, expr = vals
		self.locals_seen = set()
		return "{}\n\nfunc {}({}) string {{\n{}\n return {}}}".format(inlinefuncs, name, param, stmts, expr)
	def call_str_from_chars(self, vals):
		target, lenvar = vals
		return "string({}[:{}])".format(target, lenvar)

	# Types
	def type_ints(self, vals):
		typ, = vals
		return ToGo.TYPES[typ]
	def type_str(self, _vals):
		return 'string'

	# Variable
	def param(self, vals):
		var, typ = vals
		return "{} {}".format(var, typ)
	def var(self, vals):
		p, = vals
		p = '_' if p.startswith('_') else p # unused var must be _ in go
		return p
	def var_const(self, vals):
		p, = vals
		if p in self.consts:
			return str(self.consts[p])
		return p


	# Statements
	def statements(self, vals):
		return "\n".join(vals)
	def compound_statement(self, vals):
		return "{\n" + "\n".join(vals) + "}"
	def assert_bool(self, vals):
		val, = vals
		if val == 'false':
			return 'panic("assert-unreachable")'
		return 'if !({}){{ panic("assert-error") }}'.format(val)

	def let_bool(self, vals):
		_mut, var, expr = vals
		return "var {} bool = {}".format(var, expr)
	def let_int(self, vals):
		mut, var, expr = vals
		seen = var in self.locals_seen
		self.locals_seen.add(var)
		ctype = self.TYPES['Str64'] if len(var)==3 else self.TYPES['u8'] # HACK int typing
		declare = "var {} {}".format(var, ctype)
		if TARGET_MAGIC in expr: # need to bubble up if/match expressions?
			expr_stmt = expr.replace(TARGET_MAGIC, var);
			return "{}\n{}\n".format("" if seen else declare, expr_stmt)
		return "{} = {}\n".format(var if seen else declare, expr)
	def let_int_as(self, vals):
		var, expr, ctype = vals
		return "var {0} {1} = {1}({2})".format(var, ctype, expr)
	def let_iter(self, vals):
		_mut, val = vals
		return val
	def let_array(self, vals): # HARDCODED
		_mut, var, size, repeat = vals
		assert size == repeat
		return "var {} [{}]byte".format(var, size) # arrayz zero by default FIXME need declare?

	def _assign_op(self, vals, op):
		target, val = vals
		return "{} {} {}".format(target, op, val)
	def assign_or(self, vals):
		target, val = vals
		if len(val) != 3 and len(target) == 3:
			val = "Str64({})".format(val) # HACK int typing
		return "{} |= {}".format(target, val)
	def assign_var(self, vals):
		target, val = vals
		return "{} = {}".format(target, val)
	def assign_array(self, vals):
		target, idx, val = vals
		return "{}[{}] = {}".format(target, idx, val)

	# Control
	def if_statement(self, vals):
		if len(vals) == 2:
			cond, if_then = vals
			return "if ({}) {}".format(cond, if_then)
		cond, if_then, if_else = vals
		return "if {} {} else {}".format(cond, if_then, if_else)

	def for_statement(self, vals):
		var, chars, stmt = vals
		return f"for i := 0; i < len({chars}); i++ {{ var {var} = {chars}[i]; {stmt} }}"

	def if_int(self, vals):
		cond, i_then, i_else = vals
		# TARGET_MAGIC is propagated to output
		return "if {} {{ {} }} else {{ {} }}".format(cond, i_then, i_else)
	def expr_int_in_if(self, vals):
		expr, = vals
		return "{} = {}\n".format(TARGET_MAGIC, expr)

	def match_int(self, vals):
		var, match_head = vals
		target = TARGET_MAGIC
		return self._go_match_int_rec(target, var, match_head)
	def _go_match_int_rec(self, target, var, head):
		if isinstance(head, str):
			return "{{\n {} = {} }}".format(target, head) # else (_) branch
		guard, then_v, tail = head
		test = self._guard_var(var, guard)
		ftail = self._go_match_int_rec(target, var, tail)
		return "if {} {{\n {} = {}\n}} else {}".format(test, target, then_v, ftail)
	def _guard_var(self, var, value):
		if value == "_":
			return None # true
		if isinstance(value, tuple):
			from_v, to_v = value
			return "{0} <= {1} && {1} <= {2}".format(from_v, var, to_v)
		return "{} == {}".format(var, value)

	# Boolean
	def expr_bool(self, vals):
		if len(vals) == 2:
			a, b = vals
			return "{}||{}".format(a, b)
		val, = vals
		return val
	def and_bool(self, vals):
		if len(vals) == 3:
			a, b, c = vals
			return "({}&&{}&&{})".format(a, b, c)
		if len(vals) == 2:
			a, b = vals
			return "({}&&{})".format(a, b)
		val, = vals
		return val
	def not_bool(self, vals):
		expr, = vals
		return "!{}".format(expr)
	def _bool_op(self, vals, op):
		a, b = vals
		return "{} {} {}".format(a, op, b)

	# Expression
	def expr_int(self, vals):
		return self._binary_op(vals, '|')
	def and_term(self, vals):
		return self._binary_op(vals, '&', parens=True)
	def shifts(self, vals):
		op_a, op, op_b = vals
		return "({} {} {})".format(op_a, op, op_b)
	def converted(self, vals):
		if len(vals) == 2:
			conv, astype = vals
			return "{}({})".format(astype, conv)
		val, = vals
		return val

	# Call
	def call(self, vals):
		val, = vals
		return val
	def call_func(self, vals):
		cname, cparams = vals
		return "{}{}".format(cname, cparams)
	def call_func_u64_from(self, vals):
		cparams, = vals
		return "{}({})".format('Str64', cparams)
	def call_params(self, vals):
		return "({})".format(",".join(vals))

	# Literals
	def literal_bool(self, vals):
		val, = vals
		return val

	# Top
	def start(self, vals):
		head = ['package str64']
		foot = []
		return "\n".join(ln for ln in head + vals + foot if ln)

	@staticmethod
	def make_tests():
		header = [
			'package str64',
			'import "testing"']
		exact = [
			'func TestExact(t *testing.T) {',
			'	if encode("") != 0 { t.Error("Bad encoding for empty string") }',
			'	var exacts = []string {' + ','.join('"{}"'.format(ex) for ex in EXACT) + '}',
			'	for _, in := range exacts {',
			'		out := encode(in)',
			'		if is_lossy(out) { t.Errorf("Expected exact for %s", in) }',
			'		back := decode(out)',
			'		if back != in { t.Errorf("Round trip failed got %s for %s", back, in) }',
			'	}', '}']
		lossy = [
			'func TestLossy(t *testing.T) {',
			'	var lossies = []struct{in string; enc string}{' + ','.join('{{"{}", "{}"}}'.format(sin,enc) for (sin,enc) in LOSSY) + '}',
			'	for _, ltest := range lossies {',
			'		out := encode(ltest.in)',
			'		if !is_lossy(out) { t.Errorf("Expected lossy for %s", ltest.in) }',
			'		back := decode(out)',
			'		if back != ltest.enc { t.Errorf("Round trip failed got %s for %s", back, ltest.enc) }',
			'	}', '}']
		with open("../go/str64_test.go", "w") as f:
			for ln in header + exact + lossy:
				f.write(ln+'\n')
