""" Test strings to generate test drivers for generated languages """
EXACT = [
	"a",
	"aj",
	"_.~",
	"a~-",
	"The",
	"fast",
	"brown",
	".FOX.",
	"jumps",
	"overthe",
	"lazy.dog",
	"soooooo",
	"quickquick",
	"The", "quick", "brown", "fox", "jumps", "over", "the", "lazy","dog.",
	"leetLEAK_",
	"LEET.leak",
	"leet-LEK",
	"potatomash",
	"PotatomaSh",
	"POTATOMA",
	"MSFT", "google.com", "~jack", "smallCamel", "MAGIC_TO", "Kennedy", "username"]

LOSSY = [
	("accomplishment", "accomplis"),
	("Καλημέρα World", "World"),
	("with, it?", "withit")]
