""" Ad-hoc partial rust parser to convert str63::lib.rs to other languages """

from lark import Lark
from to_c import ToC
from to_eiffel import ToEiffel
from to_go import ToGo

RUST_SYNTAX = "./minirust.lark"
RUST_SOURCE = "../rust/src/lib.rs"


def generate(trans, parsed, outfile):
	code = trans.transform(parsed)
	with open(outfile, "w") as f:
		f.write(code)
	trans.make_tests()

if __name__ == "__main__":
	with open(RUST_SYNTAX) as rs:
		parser = Lark(rs)
	with open(RUST_SOURCE) as rf:
		lines = rf.read()
		parsed = parser.parse(lines)
		#print(parsed.pretty())
		generate(ToC(), parsed, "str64.h")
		generate(ToEiffel(), parsed, "str64.e")
		generate(ToGo(), parsed, "../go/str64.go")
		# (ada)
		# (oberon)
		# go
		# kotlin
		# (java)
		# swift
		# assemblyscript
		# (ESxx)

