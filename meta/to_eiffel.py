""" Convert reference Rust code to Eiffel """
#from lark import Transformer
from data_for_tests import EXACT, LOSSY
from to_c import ToC

class ToEiffel(ToC):
	""" Lark parse tree transformer """
	TYPES = {'bool': 'BOOLEAN',
#		'char': 'char',
		'i64': 'INTEGER_64',
		'u8': 'NATURAL_8',
		'usize': 'INTEGER',
		'Str63': 'INTEGER_64'}

	def __init__(self):
		self.consts = {}
		self.local_types = {}
	
	def _local_variables(self):
		""" Format and flush local variables """
		locals_fmt = "\n".join(["{}: {}".format(var, self.local_types[var]) for var in self.local_types])
		self.local_types.clear()
		return locals_fmt

	# collect constants for later resolving
	def const(self, vals):
		name, ctype, value = vals
		self.consts[str(name)] = int(value)
		return ""
	def const_add(self, vals):
		name, added = vals
		return self.consts[str(name)] + int(added)

	# define Str63
	def typedef_str63(self, vals):
		actual_t, = vals
		return ""
		#FIXMEreturn "typedef {} Str63;".format(actual_t)


	# Generic functions
	def func_int(self, vals):
		name, callp, rettyp, body = vals
		locs = self._local_variables() # consume just in case
		return ""
	def func_name(self, vals):
		rawname, = vals
		return rawname
	def func_body(self, vals):
		stmts = vals[:-1]
		ret = vals[-1]
		stmts_fmt = " ".join(stmts)
		return "do {}\nResult := {} end".format(stmts_fmt, ret)

	# Hardcoded functions
	def func_opt_str63(self, vals):
		name, _param, _stmts, _ret = vals
		assert name.endswith('encode_opt')
		return "" # skip
	def if_opt(self, vals):
		cond, var = vals
		return "{}?-1:{}".format(cond, var)
	def func_str63_bool(self, vals):
		name, param, stmts, tupl0, tupl1 = vals
		comments = {'encode': 'Encode string to STR63 value'}
		comment = comments[name]
		locs = self._local_variables()
		return "	{} ({})\n		-- {}\n local {}\n do	{}\n	value := {}\n	is_lossy := {}\nend\n".format(name, 
																				param, comment,locs, stmts, tupl0, tupl1)
	def func_string(self, vals):
		name, param, stmts, expr = vals
		locs = self._local_variables()
		if name.endswith('naive_string'): # not applicable in C
			return ""
		return "\n".join(["	as_string: STRING_8",
			"	-- Regular string from STR63-encoded value.",
			"	local {}\n	{}\n	do create Result.make(12)\n	{} := value {}\n	end\n".format(param, locs, param.split(':')[0], stmts)])
	def func_char12(self, vals):
		name, param, body = vals
		return ""
		# FIXME? return "static void {}({}, char chars[13]) {{{}}}".format(name, param, body)
	def func_char12_body(self, vals):
		table, cond, then_ch, then_len, e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11 = vals
		assert then_len == '12'
		then_stmt = "chars[0] = 0;"
		else_format = " ".join(["chars[{}]={{}};".format(i) for i in range(12)])
		else_stmt = else_format.format(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11)
		return ""
	def const_bytes(self, vals):
		var, lit = vals
		self.local_types[var] = 'STRING_8'
		return "{} := {};".format(var, lit)
	def char12_item(self, vals):
		arr, idx = vals
		return "{}[{}]".format(arr, idx)
	def call_str_from_chars(self, vals):
		target, lenvar = vals
		return "{}[{}] = 0;".format(target, lenvar)

	# Types
	def type_ints(self, vals):
		typ, = vals
		return ToEiffel.TYPES[typ]
	def type_str(self, _vals):
		return 'attached READABLE_STRING_GENERAL'

	# Variable
	def param(self, vals):
		var, typ = vals
		return "{}: {}".format(var, typ)
	def var(self, vals):
		p, = vals
		return p
	def var_const(self, vals):
		p, = vals
		if p in self.consts:
			return str(self.consts[p])
		return p


	# Statements
	def statements(self, vals):
		return "\n".join(vals)
	def compound_statement(self, vals):
		return "\n".join(vals)
	def assert_bool(self, vals):
		val, = vals
		return "check {} end".format(val)
	def let_mut(self, vals):
		return len(vals) == 1 # mut present
	def let_bool(self, vals):
		_mut, var, expr = vals
		self.local_types[var] = 'BOOLEAN'
		return "{} := {}".format(var, expr)
	def let_int(self, vals):
		_mut, var, ctype, expr = vals
		self.local_types[var] = ctype
		return "{} := {}".format(var, expr)
	def let_int_gen(self, vals):
		_mut, var, expr = vals
		self.local_types[var] = 'INTEGER'
		return "{} := {}".format(var, expr)
	def let_int_as(self, vals):
		var, expr, ctype = vals
		self.local_types[var] = ctype
		return "{} := {}.as_{}".format(var, expr, ctype.lower())
	def const_64(self, vals):
		var, varin = vals
		self.local_types[var] = 'INTEGER_64'
		return "{} := {}".format(var, varin) # FIXME requires cast?
	def let_iter(self, vals):
		_mut, val = vals
		return val
	def let_array(self, vals): # HARDCODED
		_mut, var, size, repeat = vals
		assert size == repeat
		assert var == 'chars'
		return "" # STRING manages initialisation
	def let_str63_bool(self, vals):
		v0, v1, cname, cparam = vals
		if v1.startswith('_'): # bool not used
			return "Str63 {0}; {1}({2}, &{0});".format(v0, cname, cparam)
		return "Str63 {0}; _Bool {1} := {2}({3}, &{0});".format(v0, v1, cname, cparam)

	def assign_or(self, vals):
		return self._assign_op(vals, "|")
	def assign_and(self, vals):
		return self._assign_op(vals, "&")
	def assign_add(self, vals):
		return self._assign_op(vals, "+")
	def assign_bool(self, vals):
		target, val = vals
		self.local_types[target] = 'BOOLEAN'
		return "{} := {}".format(target, val)
	def _assign_op(self, vals, op):
		target, val = vals
		return "{0} := {0} {1} {2}".format(target, op, val)
	def assign_var(self, vals):
		target, val = vals
		return "{} := {}".format(target, val)
	def assign_array(self, vals):
		target, idx, val = vals
		return " Result.append_code({})".format(val)

	# Control
	def if_statement(self, vals):
		cond, if_then, if_else = vals
		return "if {}\nthen {}\nelse {} end".format(cond, if_then, if_else)

	def if_int(self, vals):
		cond, i_then, i_else = vals
		return "NOTUSED"

	def while_statement(self, vals):
		test, stmt = vals
		return "from until not ({}) loop\n	{}\nend".format(test, stmt)

	def match_3(self, vals):
		assert len(vals) > 4
		mvars = vals[:3]
		else_stmt = vals[-1]
		fbranches = "\n else".join(self._match_3_item(mvars, branch) for branch in vals[3:-1])
		return "{}\n else {}\n end".format(fbranches, else_stmt)
	def _match_3_item(self, mvars, branch):
		var1, var2, var3 = mvars
		guard1, guard2, guard3, stmt = branch
		g1 = self._guard(var1, var2, var3, guard1)
		g2 = self._guard(var1, var2, var3, guard2)
		g3 = self._guard(var1, var2, var3, guard3)
		if g3 is not None:
			return "if ({}) or else ({}) or else ({}) then\n	{}".format(g1, g2, g3, stmt)
		if g2 is not None:
			return "if ({}) or else ({}) then\n	{}".format(g1, g2, stmt)
		return "if {} then\n	{}".format(g1, stmt)
	def _guard(self, var1, var2, var3, value):
		if value is None:
			return None
		match1, match2, match3 = value
		test1 = self._guard_var(var1, match1)
		test2 = self._guard_var(var2, match2)
		test3 = self._guard_var(var3, match3)
		tests = ["({})".format(t) for t in [test1, test2, test3] if t]
		return " and ".join(tests)
	def _guard_var(self, var, value):
		if value == "_":
			return None # true
		if isinstance(value, tuple):
			from_v, to_v = value
			return "{0} <= {1} and {1} <= {2}".format(from_v, var, to_v)
		return "{} = {}".format(var, value)

	def let_match_int(self, vals):
		_mut, target, ctype, var, match_head = vals
		matches = self._e_match_int_rec(target, var, match_head)
		self.local_types[target] = 'NATURAL_32'
		return matches
	def match_int_branches(self, vals):
		guard, then_v, tail = vals
		return (guard, then_v, tail)
	def _e_match_int_rec(self, target, var, head):
		if isinstance(head, str):
			return " {} := {} end".format(target, head) # else (_) branch
		guard, then_v, tail = head
		test = self._guard_var(var, guard)
		ftail = self._e_match_int_rec(target, var, tail)
		return "if {} then {} := {}\n	else{}".format(test, target, then_v, ftail)

	# Boolean
	def expr_bool(self, vals):
		if len(vals) == 2:
			a, b = vals
			return "{} or else {}".format(a, b)
		val, = vals
		return val
	def and_bool(self, vals):
		if len(vals) == 3:
			a, b, c = vals
			return "({} and then {} and then {})".format(a, b, c)
		if len(vals) == 2:
			a, b = vals
			return "({} and then {})".format(a, b)
		val, = vals
		return val
	def not_bool(self, vals):
		expr, = vals
		return "not {}".format(expr)
	def equal(self, vals):
		return self._bool_op(vals, ' = ')
	def notequal(self, vals):
		return self._bool_op(vals, ' /= ')
	def _bool_op(self, vals, op):
		a, b = vals
		return "{}{}{}".format(a, op, b)

	# Expression
	def expr_int(self, vals):
		return self._binary_op(vals, ' | ')
	def and_term(self, vals):
		return self._binary_op(vals, ' & ', parens=True)
	def shifts(self, vals):
		if len(vals) == 3:
			op_a, op, op_b = vals
			return "({} |{} {})".format(op_a, op, op_b)
		val, = vals
		return val
	def converted(self, vals):
		if len(vals) == 2:
			conv, astype = vals
			return "({}) {}".format(astype, conv)
		val, = vals
		return val
	def expr_int_parens(self, vals):
		val, = vals
		return "({})".format(val)
	def _binary_op(self, vals, op, parens=False):
		if len(vals) > 1:
			return ("({})" if parens else "{}").format(op.join(vals))
		val, = vals
		return val

	# Call
	def call(self, vals):
		val, = vals
		return val
	def iter_init(self, vals):
		_it_target, _src, next_target, _it_src  = vals
		self.local_types[next_target] = 'INTEGER'
		return "{} := 1".format(next_target)
	def iter_next(self, vals):
		target, next_src, next_target, it = vals
		self.local_types[target] = 'NATURAL_32'
		return "{0} := s.code({1})\n{1} := {1} + 1".format(target, next_src)
	def call_is_some(self, vals):
		var, = vals
		return "({} <= s.count)".format(var) # FIXME 's' func param hardcoded
	def call_func(self, vals):
		cname, cparams = vals
		return "{}{}".format(cname, cparams)
	def call_func_i64_from(self, vals):
		cparams, = vals
		etyp = ToEiffel.TYPES["i64"]
		return "{}.as_{}".format(cparams, etyp.lower())
	def call_params(self, vals):
		return "({})".format(",".join(vals))

	# Literals
	def literal_dec(self, vals):
		val, = vals
		return val
	def literal_bool(self, vals):
		val, = vals
		return {'true': 'True', 'false': 'False'}[val]
	def literal_bytes(self, vals):
		ch, = vals
		return "\"{}\"".format(ch)

	# Top
	def start(self, vals):
		head = [
			'note',
			'	description: "Encoder/decoder for STR63 tokens"',
			'	machine_generated: "This code is cross-compiled from the Rust STR63 implementation"',
			'class STR63', 
			'create',
				'	encode, from_value', 
			'feature {ANY}', 
				'	value: INTEGER_64','		-- Token encoded using the STR63 method',
				'	is_lossy: BOOLEAN', '		-- Has last encoding skipped some characters from input string?',
			'feature {ANY}',
				'is_value(v: INTEGER_64): BOOLEAN', '		-- Valid encoded value?',
				'	do Result := v > 0x4000000000000000 end',
				'from_value(v: INTEGER_64)', '		-- Initialize from already encoded value',
					'require valid_encoding: is_value(v)',
					'	do value := v end']
		foot = ['invariant valid_encoding: is_value(value)', 'end']
		return "\n".join(ln for ln in head + vals + foot if ln)
	def tests(self, _vals):
		return ""

	@staticmethod
	def make_tests():
		header = [
			'class TEST_STR63',
			'create make',
			'feature {NONE} make',
			'	local maker: STR63',
			'do',
			'	create maker.encode("")'
			'	check maker.value = 0x4fffffffffffffff end']
		exact = [
			'	maker.encode("{0}"); check not maker.is_lossy; maker.as_string.same_string("{0}") end'
			.format(ex) for ex in EXACT]
		lossy = [
			'	maker.encode("{0}"); check maker.is_lossy; maker.as_string.same_string("{1}") end'
			.format(s, so) for (s, so) in LOSSY]
		footer = ['	end', 'end']
		with open("test_str63.e", "w") as f:
			for ln in header + exact + lossy + footer:
				f.write(ln+'\n')
