""" Convert reference Rust code to C """
from lark import Transformer
from data_for_tests import EXACT, LOSSY

class ToC(Transformer):
	""" Lark parse tree transformer """
	TYPES = {'bool': '_Bool',
		'char': 'char',
		'u64': 'uint64_t',
		'u8': 'uint8_t',
		'usize': 'size_t',
		'Str64': 'Str64'}

	def __init__(self):
		self.consts = {}

	# collect constants for later resolving
	def const(self, vals):
		name, ctype, value = vals
		self.consts[str(name)] = int(value)
		return ""
	def const_add(self, vals):
		name, added = vals
		return self.consts[str(name)] + int(added)

	# define Str64
	def typedef_us(self, vals):
		actual_t, = vals
		return "typedef {} Str64;".format(actual_t)


	# Generic functions
	def func_int(self, vals):
		name, callp, rettyp, body = vals
		self.locals_seen = set()
		return "static {} {}({}) {}".format(rettyp, name, callp, body)
	def func_bool(self, vals):
		name, param, stmts, ret = vals
		self.locals_seen = set()
		return "static {} {}({}) {{ {} return {}; }}".format(self.TYPES["bool"], name, param, stmts, ret)
	def func_name(self, vals):
		rawname, = vals
		return "str64_{}".format(rawname)
	def func_params(self, vals):
		return ",".join(vals)
	def func_body(self, vals):
		assert len(vals) == 2 # FIXME fixed
		stmts = vals[:-1]
		ret = vals[-1]
		stmts_fmt = " ".join(stmts)
		return "{{{} return {};}}".format(stmts_fmt, ret)
	def func_defs(self, vals):
		return " ".join(vals)

	# Hardcoded functions
	def if_opt(self, vals):
		cond, var = vals
		return "{}?-1:{}".format(cond, var)
	def func_string(self, vals):
		name, param, inlinefuncs, stmts, expr = vals
		self.locals_seen = set()
		return "{} static void {}({}, char chars[17]) {{{} {}}}".format(inlinefuncs, name, param, stmts, expr)
	def call_str_from_chars(self, vals):
		target, lenvar = vals
		return "{}[{}] = 0;".format(target, lenvar)

	# Types
	def type_ints(self, vals):
		typ, = vals
		return ToC.TYPES[typ]
	def type_str(self, _vals):
		return 'char*'

	# Variable
	def param(self, vals):
		var, typ = vals
		return "{} {}".format(typ, var)
	def var(self, vals):
		p, = vals
		return p
	def var_const(self, vals):
		p, = vals
		if p in self.consts:
			return str(self.consts[p])
		return p


	# Statements
	def statement(self, vals):
		val,  = vals
		return val
	def statements(self, vals):
		return " ".join(vals)
	def compound_statement(self, vals):
		return "{" + " ".join(vals) + "}"
	def assert_bool(self, vals):
		val, = vals
		return "assert({});".format(val)
	def let_mut(self, vals):
		return len(vals) == 1 # mut present
	def let_bool(self, vals):
		_mut, var, expr = vals
		return "_Bool {} = {};".format(var, expr)
	def let_int(self, vals):
		mut, var, expr = vals
		if var in self.locals_seen: # already declared
			return "{} = {};".format(var, expr)
		self.locals_seen.add(var)
		ctype = self.TYPES['u64'] if mut else self.TYPES['u8'] # FIXME too ad hoc
		return "{} {} = {};".format(ctype, var, expr)
	def let_int_as(self, vals):
		var, expr, ctype = vals
		return "{0} {1} = ({0}) {2};".format(ctype, var, expr)
	def let_iter(self, vals):
		_mut, val = vals
		return val
	def let_array(self, vals): # HARDCODED
		_mut, var, size, repeat = vals
		assert size == repeat
		# initialize characters and final 0
		return " ".join(["{}[{}] = 0;".format(var, i) for i in range(int(repeat)+1)])

	def assign_or(self, vals):
		return self._assign_op(vals, "|=")
	def assign_add(self, vals):
		return self._assign_op(vals, "+=")
	def assign_bool(self, vals):
		return self._assign_op(vals, "=")
	def _assign_op(self, vals, op):
		target, val = vals
		return "{} {} {};".format(target, op, val)
	def assign_var(self, vals):
		target, val = vals
		return "{} = {};".format(target, val)
	def assign_array(self, vals):
		target, idx, val = vals
		return "{}[{}] = {};".format(target, idx, val)

	# Control
	def if_statement(self, vals):
		if len(vals) == 2:
			cond, if_then = vals
			return "if ({}) {}".format(cond, if_then)
		cond, if_then, if_else = vals
		return "if ({}) {} else {}".format(cond, if_then, if_else)

	def if_int(self, vals):
		cond, i_then, i_else = vals
		return "{}?{}:({})".format(cond, i_then, i_else)
	def expr_int_in_if(self, vals):
		expr, = vals
		return expr

	def if_bool(self, vals):
		cond, if_stmts, then_bool, else_bool = vals
		if if_stmts: # using comma operator for inline statements
			return "{}?({},{}):({})".format(cond, if_stmts, then_bool, else_bool)
		return "{}?({}):({})".format(cond, then_bool, else_bool)
	def if_statements(self,vals):
		return ",".join(sv[:-1] for sv in vals)

	def for_statement(self, vals):
		var, chars, stmt = vals
		return f"for(int i=0; i < strlen({chars}); i++) {{ unsigned char {var} = {chars}[i]; {stmt} }}"


	def match_range(self, vals):
		from_v, to_v = vals
		return (from_v, to_v)

	def match_int(self, vals):
		var, match_head = vals
		return self._match_int_rec(var, match_head)
	def match_int_branches(self, vals):
		guard, then_v, tail = vals
		return (guard, then_v, tail)
	def _match_int_rec(self, var, head):
		if isinstance(head, str):
			return head # else (_) branch
		guard, then_v, tail = head
		test = self._guard_var(var, guard)
		ftail = self._match_int_rec(var, tail)
		return "({}) ? {} : ({})".format(test, then_v, ftail)
	def _guard_var(self, var, value):
		if value == "_":
			return None # true
		if isinstance(value, tuple):
			from_v, to_v = value
			return "{0} <= {1} && {1} <= {2}".format(from_v, var, to_v)
		return "{} == {}".format(var, value)
	def match_int_branches(self, vals):
		return vals
	def match_int_expr(self, vals):
		guard, expr = vals
		return (guard, expr)
	def match_int_else(self, vals):
		expr, = vals
		return expr

	# Boolean
	def expr_bool(self, vals):
		if len(vals) == 2:
			a, b = vals
			return "{}||{}".format(a, b)
		val, = vals
		return val
	def and_bool(self, vals):
		if len(vals) == 3:
			a, b, c = vals
			return "({}&&{}&&{})".format(a, b, c)
		if len(vals) == 2:
			a, b = vals
			return "({}&&{})".format(a, b)
		val, = vals
		return val
	def not_bool(self, vals):
		expr, = vals
		return "!{}".format(expr)
	def lowerthan(self, vals):
		return self._bool_op(vals, '<')
	def lowerthaneq(self, vals):
		return self._bool_op(vals, '<=')
	def equal(self, vals):
		return self._bool_op(vals, '==')
	def notequal(self, vals):
		return self._bool_op(vals, '!=')
	def _bool_op(self, vals, op):
		a, b = vals
		return "{}{}{}".format(a, op, b)

	# Expression
	def expr_int(self, vals):
		return self._binary_op(vals, '|')
	def and_term(self, vals):
		return self._binary_op(vals, '&', parens=True)
	def shifts(self, vals):
		op_a, op, op_b = vals
		return "({}{}{})".format(op_a, op, op_b)
	def term_plus_minus(self, vals):
		op_a, op_b, op_c = vals
		return "{} + {} - {}".format(op_a, op_b, op_c)
	def term_plus(self, vals):
		op_a, op_b = vals
		return "{} + {}".format(op_a, op_b)
	def term_minus(self, vals):
		op_a, op_b = vals
		return "{} - {}".format(op_a, op_b)
	def factor(self, vals):
		return self._binary_op(vals, "*")
	def converted(self, vals):
		if len(vals) == 2:
			conv, astype = vals
			return "({}) {}".format(astype, conv)
		val, = vals
		return val
	def expr_int_parens(self, vals):
		val, = vals
		return "({})".format(val)
	def _binary_op(self, vals, op, parens=False):
		if len(vals) > 1:
			return ("({})" if parens else "{}").format(op.join(vals))
		val, = vals
		return val

	# Call
	def call(self, vals):
		val, = vals
		return val
	def call_func(self, vals):
		cname, cparams = vals
		return "{}{}".format(cname, cparams)
	def call_func_u64_from(self, vals):
		cparams, = vals
		return "({}) {}".format(ToC.TYPES["u64"], cparams)
	def call_params(self, vals):
		return "({})".format(",".join(vals))

	# Literals
	def literal_dec(self, vals):
		val, = vals
		return str(val)
	def literal_bool(self, vals):
		val, = vals
		return {'true': 1, 'false': 0}[val]

	# Top
	def start(self, vals):
		head = ['#ifndef STR64_H', '#define STR64_H', '#include <stdint.h>', '#include <stddef.h>', '#include <assert.h>']
		foot = ['#endif']
		return "\n".join(ln for ln in head + vals + foot if ln)
	def tests(self, _vals):
		return ""

	@staticmethod
	def make_tests():
		header = [
			'#include <assert.h>', '#include <string.h>', '#include "str64.h"',
			'int main() {',
			'	Str64 out; char dec[17];',
			'	out = str64_encode("");',
			'	assert(!str64_is_lossy(out)); assert(out==0);']
		exact = [
			'	out = str64_encode("{0}"); assert(!str64_is_lossy(out)); str64_decode(out, dec); assert(!strcmp(dec, "{0}"));'
			.format(ex) for ex in EXACT]
		lossy = [
			'	out = str64_encode("{0}"); assert(str64_is_lossy(out)); str64_decode(out, dec); assert(!strcmp(dec, "{1}"));'
			.format(s, so) for (s, so) in LOSSY]
		footer = ["}"]
		with open("test.c", "w") as f:
			for ln in header + exact + lossy + footer:
				f.write(ln+'\n')
