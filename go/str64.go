package str64

type Str64 uint64

func is_lossy(es Str64) bool { return (es & 255) == 255 }
func encode(s string) Str64 {
	var lossy bool = false
	var enc Str64 = 0

	for i := 0; i < len(s); i++ {
		var ch = s[i]
		{
			if (enc >> 56) == 0 {
				var lowch uint8
				if ch == 97 {
					lowch = 1
				} else if ch == 101 {
					lowch = 2
				} else if ch == 105 {
					lowch = 3
				} else if ch == 111 {
					lowch = 4
				} else if ch == 117 {
					lowch = 5
				} else if ch == 121 {
					lowch = 6
				} else {
					lowch = 0
				}

				if lowch != 0 && (enc&7) == 0 {
					enc |= Str64(lowch)
				} else {
					var upper bool = (65 <= ch && ch <= 90)
					var e1 uint8
					if upper {
						e1 = 1 + ch - 65
					} else {
						if 97 <= ch && ch <= 122 {
							e1 = 1 + ch - 97
						} else {
							if ch == 45 {
								e1 = 27
							} else if ch == 46 {
								e1 = 28
							} else if ch == 95 {
								e1 = 29
							} else if ch == 126 {
								e1 = 30
							} else if ch == 33 {
								e1 = 31
							} else {
								e1 = 0
							}
						}
					}

					if e1 == 0 {
						lossy = true
					} else {
						var b1 uint8
						if upper {
							b1 = (e1 << 3) | 7
						} else {
							b1 = (e1 << 3)
						}

						enc = (enc << 8) | Str64(b1)
					}
				}
			} else {
				lossy = true
			}
		}
	}
	if lossy {
		if (enc >> 56) == 0 {
			enc = (enc << 8)
		}
		enc = enc | 255
	}
	return enc
}
func char5(ev Str64) uint8 {
	var eb uint8 = uint8(((ev >> 3) & 31))
	var ch uint8
	if eb == 0 {
		ch = 0
	} else if eb == 27 {
		ch = 45
	} else if eb == 28 {
		ch = 46
	} else if eb == 29 {
		ch = 95
	} else if eb == 30 {
		ch = 126
	} else if eb == 31 {
		ch = 33
	} else {
		ch = 97 + eb - 1
	}

	var ret00 uint8 = 0
	if (ev & 7) == 7 {
		if 97 <= ch && ch <= 122 {
			ret00 = ch - 32
		} else {
			ret00 = 0
		}
	} else {
		ret00 = ch
	}
	return ret00
}
func char3(ev Str64) uint8 {

	var ret00 uint8 = 0
	if (ev & 7) == 1 {
		ret00 = 97
	} else if (ev & 7) == 2 {
		ret00 = 101
	} else if (ev & 7) == 3 {
		ret00 = 105
	} else if (ev & 7) == 4 {
		ret00 = 111
	} else if (ev & 7) == 5 {
		ret00 = 117
	} else if (ev & 7) == 6 {
		ret00 = 121
	} else {
		ret00 = 0
	}
	return ret00
}

func decode(enc Str64) string {
	var chars [16]byte
	var widx uint8 = 0

	var ech Str64 = (enc >> 56)

	var ch uint8 = char5(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ech = (enc >> 48)

	ch = char5(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ech = (enc >> 40)

	ch = char5(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ech = (enc >> 32)

	ch = char5(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ech = (enc >> 24)

	ch = char5(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ech = (enc >> 16)

	ch = char5(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ech = (enc >> 8)

	ch = char5(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(ech)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char5(enc)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	ch = char3(enc)

	if ch != 0 {
		chars[widx] = ch
		widx += 1
	}
	return string(chars[:widx])
}
