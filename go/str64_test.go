package str64
import "testing"
func TestExact(t *testing.T) {
	if encode("") != 0 { t.Error("Bad encoding for empty string") }
	var exacts = []string {"a","aj","_.~","a~-","The","fast","brown",".FOX.","jumps","overthe","lazy.dog","soooooo","quickquick","The","quick","brown","fox","jumps","over","the","lazy","dog.","leetLEAK_","LEET.leak","leet-LEK","potatomash","PotatomaSh","POTATOMA","MSFT","google.com","~jack","smallCamel","MAGIC_TO","Kennedy","username"}
	for _, in := range exacts {
		out := encode(in)
		if is_lossy(out) { t.Errorf("Expected exact for %s", in) }
		back := decode(out)
		if back != in { t.Errorf("Round trip failed got %s for %s", back, in) }
	}
}
func TestLossy(t *testing.T) {
	var lossies = []struct{in string; enc string}{{"accomplishment", "accomplis"},{"Καλημέρα World", "World"},{"with, it?", "withit"}}
	for _, ltest := range lossies {
		out := encode(ltest.in)
		if !is_lossy(out) { t.Errorf("Expected lossy for %s", ltest.in) }
		back := decode(out)
		if back != ltest.enc { t.Errorf("Round trip failed got %s for %s", back, ltest.enc) }
	}
}
