#ifndef STR64_H
#define STR64_H
#include <stdint.h>
#include <stddef.h>
#include <assert.h>
typedef uint64_t Str64;
static _Bool
str64_is_lossy (Str64 es)
{
  return (es & 255) == 255;
}

static Str64
str64_encode (char *s)
{
  _Bool lossy = 0;
  uint64_t enc = 0;
  for (int i = 0; i < strlen (s); i++) {
    unsigned char ch = s[i]; {
      if ((enc >> 56) == 0) {
	uint8_t lowch = (ch == 97) ? 1 : ((ch == 101) ? 2 : ((ch == 105) ? 3 : ((ch == 111) ? 4 : ((ch == 117) ? 5 : ((ch == 121) ? 6 : (0))))));
	if ((lowch != 0 && (enc & 7) == 0)) {
	  enc |= lowch;
	}
	else {
	  _Bool upper = (65 <= ch && ch <= 90);
	  uint8_t e1 = upper ? 1 + ch - 65 : ((97 <= ch && ch <= 122) ? 1 + ch - 97 : ((ch == 45) ? 27 : ((ch == 46) ? 28 : ((ch == 95) ? 29 : ((ch == 126) ? 30 : ((ch == 33) ? 31 : (0)))))));
	  if (e1 == 0) {
	    lossy = 1;
	  }
	  else {
	    uint8_t b1 = upper ? (e1 << 3) | 7 : ((e1 << 3));
	    enc = (enc << 8) | (uint64_t) b1;
	  }
	}
      }
      else {
	lossy = 1;
      }
    }
  }
  if (lossy) {
    if ((enc >> 56) == 0) {
      enc = (enc << 8);
    }
    enc = enc | 255;
  }
  return enc;
}

static uint8_t
str64_char5 (uint64_t ev)
{
  uint8_t eb = (uint8_t) (((ev >> 3) & 31));
  uint8_t ch = (eb == 0) ? 0 : ((eb == 27) ? 45 : ((eb == 28) ? 46 : ((eb == 29) ? 95 : ((eb == 30) ? 126 : ((eb == 31) ? 33 : (97 + eb - 1))))));
  return (ev & 7) == 7 ? (97 <= ch && ch <= 122) ? ch - 32 : (0) : (ch);
}

static uint8_t
str64_char3 (uint64_t ev)
{
  return ((ev & 7) == 1) ? 97 : (((ev & 7) == 2) ? 101 : (((ev & 7) == 3) ? 105 : (((ev & 7) == 4) ? 111 : (((ev & 7) == 5) ? 117 : (((ev & 7) == 6) ? 121 : (0))))));
}

static void
str64_decode (Str64 enc, char chars[17])
{
  chars[0] = 0;
  chars[1] = 0;
  chars[2] = 0;
  chars[3] = 0;
  chars[4] = 0;
  chars[5] = 0;
  chars[6] = 0;
  chars[7] = 0;
  chars[8] = 0;
  chars[9] = 0;
  chars[10] = 0;
  chars[11] = 0;
  chars[12] = 0;
  chars[13] = 0;
  chars[14] = 0;
  chars[15] = 0;
  chars[16] = 0;
  uint64_t widx = 0;
  uint8_t ech = (enc >> 56);
  uint8_t ch = str64_char5 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ech = (enc >> 48);
  ch = str64_char5 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ech = (enc >> 40);
  ch = str64_char5 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ech = (enc >> 32);
  ch = str64_char5 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ech = (enc >> 24);
  ch = str64_char5 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ech = (enc >> 16);
  ch = str64_char5 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ech = (enc >> 8);
  ch = str64_char5 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (ech);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char5 (enc);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  ch = str64_char3 (enc);
  if (ch != 0) {
    chars[widx] = ch;
    widx += 1;
  }
  chars[widx] = 0;
}
#endif
