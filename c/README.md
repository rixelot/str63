# C header library for STR64 encoding/decoding

This is a header library, to be included for use:

```
#include <str64.h>
```

It defines an alias for 64 bit values used to store STR64-encoded short
string.

```
typedef uint64_t Str64;
```

See STR64.md for the definition of the encoding.

All functions are static.

## Encoding

```
Str64 str64_encode(char* s)
```

This encodes the input string s and returns a Str64 value.

It the input is too long, or contains input characters that are 
not supported, the value returned encodes on a best effort basis 
and includes a marker to indicate that the encoding was lossy.

This marker can be checked with:
```
_Bool str64_is_lossy(Str64 s)
```

## Decoding

```
void str64_decode(Str64 enc, char chars[17])
```

Decodes a STR64-encoded value into an ASCII zero-terminated C string. 
Up to 17 bytes can be written, including the final zero, as each byte 
of the input can encode up to 2 ASCII characters.

The decoding cannot fail because there is no invalid value.

## Generated code

This library-header is generated based on a transform of the Rust reference 
code, which may be easier to read.

## No copyright

This work is in the public domain.
