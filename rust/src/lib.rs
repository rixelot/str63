// See STR64.md for format description

// Type is just a 64bit integer
pub type Str64 = u64;

// ascii input
const LOW_A:u8 = 97;
const LOW_E:u8 = 101;
const LOW_I:u8 = 105;
const LOW_O:u8 = 111;
const LOW_U:u8 = 117;
const LOW_Y:u8 = 121;
const LOW_Z:u8 = LOW_A + 25;
const UPP_A:u8 = 65;
const UPP_Z:u8 = UPP_A + 25;

// punctuation signs that are URI safe
const EXCL:u8 = 33; // !
const MINU:u8 = 45; // -
const BDOT:u8 = 46; // .(baseline dot)
const UNDR:u8 = 95; // _
const TILD:u8 = 126; // ~

/// Was the source of this token something more than the decoded version?
pub fn is_lossy(es:Str64) -> bool {
	es & 255 == 255
}

/// Encode string to Str64, return it and lossy status
pub fn encode(s: &str) -> Str64 {
	let mut lossy = false;
	let mut enc = 0;
	for ch in s.bytes() {
		if enc >> 56 == 0 { // not full
			let lowch = match ch {LOW_A=>1, LOW_E=>2, LOW_I=>3, LOW_O=>4, LOW_U=>5, LOW_Y=>6, _=>0};
			if lowch != 0 && enc & 7 == 0 { // free vowel slot in previous encoded byte
				enc |= lowch;
			}
			else {
				let upper = UPP_A <= ch && ch <= UPP_Z;
				let e1 = if upper { 1 + ch - UPP_A }
					else { 
						if LOW_A <= ch && ch <= LOW_Z { 1 + ch - LOW_A }
						else { match ch {MINU=>27, BDOT=>28, UNDR=>29,TILD=>30,EXCL=>31, _=>0} } };
				if e1 == 0 { lossy = true; }
				else {
					let b1 = if upper { e1 << 3 | 7 } else { e1 << 3 };
					enc = enc << 8 | u64::from(b1);
				}
			}
		}
		else { lossy = true; } // full
	}
	if lossy {
		if enc >> 56 == 0 { enc = enc << 8; } // push if spare byte
		enc = enc | 255; // lowest byte is FF is lossy marker
	}
	enc
}

/// Decode Str64 to string
pub fn decode(enc: Str64) -> String {
	fn char5(ev:u64) -> u8 { let eb = (ev >> 3 & 31) as u8;
							 let ch = match eb  {0=>0, 27=>MINU, 28=>BDOT, 29=>UNDR, 30=>TILD, 31=>EXCL, _=>LOW_A+eb-1};
	                         if ev & 7 == 7 { if LOW_A <= ch && ch <= LOW_Z {ch - 32} else {0} } else {ch} }
	fn char3(ev:u64) -> u8 { match ev & 7 {1=>LOW_A, 2=>LOW_E, 3=>LOW_I, 4=>LOW_O, 5=>LOW_U, 6=>LOW_Y, _=>0} }

	// output
	let mut chars:[u8;16] = [0;16];
	let mut widx = 0;

	let ech = enc >> 56;
	let ch = char5(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ech = enc >> 48;
	let ch = char5(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ech = enc >> 40;
	let ch = char5(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ech = enc >> 32;
	let ch = char5(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ech = enc >> 24;
	let ch = char5(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ech = enc >> 16;
	let ch = char5(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ech = enc >> 8;
	let ch = char5(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(ech); if ch != 0 { chars[widx] = ch; widx += 1; }
	// last byte
	let ch = char5(enc); if ch != 0 { chars[widx] = ch; widx += 1; }
	let ch = char3(enc); if ch != 0 { chars[widx] = ch; widx += 1; }

	String::from(std::str::from_utf8(&chars[..widx]).unwrap())
}

