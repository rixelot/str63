use str64::{Str64,encode,is_lossy,decode};

#[test]
fn hello() {
	let coded = encode("Hello");
	assert!(!is_lossy(coded));
	let hand_coded:u64 = 0b_00000000_00000000_00000000_00000000_01000_111_00101_000_01100_000_01100_100;
	assert_eq!(coded, hand_coded);
	assert_eq!(decode(coded), "Hello");
}

#[test]
fn empty() {
	let subj = encode("");
	assert!(!is_lossy(subj));
	assert_eq!(subj, 0);
	assert_eq!(decode(subj), "");
}

#[test]
fn punctuation() {
	let subj = encode("-._~");
	assert!(!is_lossy(subj));
	assert_eq!(decode(subj), "-._~");
	let subj = encode("!!!");
	assert!(!is_lossy(subj));
	assert_eq!(decode(subj), "!!!");
}

fn round_trip_exact(s: &str) {
	let subj = encode(s);
	assert!(!is_lossy(subj));
	assert_eq!(decode(subj), s);
}

#[test]
fn exacts() {
	round_trip_exact("a");
	round_trip_exact("aj");
	round_trip_exact("The");
	round_trip_exact("fast");
	round_trip_exact("brown");
	round_trip_exact(".FOX.");
	round_trip_exact("jumps");
	round_trip_exact("overthe");
	round_trip_exact("lazy.dog");
	round_trip_exact("~sooooooo");
	round_trip_exact("quickquick");
}

#[test]
fn the_fox() {
	for s in &["The", "quick", "brown", "fox", "jumps", "over", "the", "lazy","dog."] {
		round_trip_exact(s);
	}
}

#[test]
fn specials_11_12() {
	round_trip_exact("leet.leak");
	round_trip_exact("leetLEAK_");
	round_trip_exact("LEET.leak");
	round_trip_exact("leet-LEK");
	round_trip_exact("potatomash");
	round_trip_exact("PotatomaSh");
	round_trip_exact("POTATOMA");
}

#[test]
fn examples() {
	round_trip_exact("MSFT");
	round_trip_exact("google.com");
	round_trip_exact("MAGIC_TO");
	round_trip_exact("foo-bar");
	round_trip_exact("Kennedy");
	round_trip_exact("~jack");
	round_trip_exact("username");
}

fn encode_force_lossy(s: &str) -> Str64 {
	let enc = encode(s);
	assert!(is_lossy(enc));
	enc
}

#[test]
fn lossy() {
	let enc = encode_force_lossy("accomplishment");
	assert_eq!(decode(enc), "accomplis");
	let enc = encode_force_lossy("Καλημέρα World");
	assert_eq!(decode(enc), "World");
	let enc = encode_force_lossy("with, it?"); // punctuation
	assert_eq!(decode(enc), "withit");
}
