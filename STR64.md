# Str64: a simple encoding of short words/tokens inside 64-bits integers

Str64 is a way to encode strings composed of ASCII letters and digits, typically tokens, 
identifiers or simple words, within a 64 bit value. It is a compromise between encoding 
complexity and capacity, offering up to 16 alphabetic characters in mixed case, making 
it materially more useful than a trivial 8 character byte-based encoding. It also ensures 
that the encoded strings are safe from containing control or extended characters.

Each byte encodes up to 2 characters. The top 5 bits of each byte encodes 31 characters, 
with 0 meaning no character (skip). The supported character are the 26 ASCII letters (a to z),
four punctuation symbols (. _ - ~) that are URL safe, and the exclamation mark (!).

The lowest 3 bit encode 6 "vowels" (a, e, i, o, u, y) and the value 111 (in binary) is 
used to enable upper case for the top 5 bits.

## Format

The characters are encoded from most significant byte as first character(s) to last 
character at the lowest significant byte.

The value 0 means no character is encoded (skip).

The byte 11111111 in the last position is used to mark that encoding was lossy (too 
long, or unsupported input characters that were dropped or modified). It decodes to
no character, like 0.

### The character set

Top 5 bits of a byte take the following values:

- 0: skip (no character)
- 1 to 26: ascii letters a to z (lower case)
- 27: - (minus sign)
- 28: . (baseline dot)
- 29: _ (underscore)
- 30: ~ (tilde)
- 31: ! (exclamation mark)

Bottom 3 bis of a byte:

- 0: skip (no character)
- 1: a (lower case latin letter a)
- 2: e
- 3: i
- 4: o
- 5: u
- 6: y
- 7: character coded by the top 5 bits is upper case (if letter)

Letters are 1 to 26, so that the lower bits match the ASCII encoding.

The punctuation characters below 31 are the same as the unreserved 
characters in URIs (RFC 3986). The exclamation mark, !, was unreserved
in previous version of the URL standard.

No bits set (a byte with the value 0) means "skip" (no full character, 
no vowel).

The URL-safe punctuation symbols  are a synonym for skip (0).

All bits set (11111111) is the lossy market (and not "upper case 
exclamation mark").

Other punctuation character combined with the upper case marker (first 5 
bits value between 27 and 30, lower 3 bits sets) are a synonym of "skip".

## Remarks

### Empty string

The canonical empty string is 0.

### String length

The maximum encodable length is 16 characters (8 vowels at even positions).

### String ordering

The values do not necessarily order in lexicographic order, if interpreted as 
unsigned integers, due to the vowels encoding and the possibility of multiple 
encoding.

If vowel encoding is not used, the first byte coded is always the same, and skip 
bytes are always at the end of the value, then integer sorting order is equivalent 
to string lexicographic sorting order.

### Multiplicity of encodings

Some strings can be encoded in multiple ways by playing with the skip 
character, the lossy marker character, and the choice between encoding 
vowels with a consonant or in their own byte.

It is though recommended to follow the following conventions:

- all the skip bytes are at the beginning of the value
- the lossy marker, if present, is the least significant byte only

### Versioning

Versioning of such formats greatly diminishes their utility, as the 
value is in the stability. Any variant of this format must be named 
differently and considered to be a new encoding.

### No copyright

This work is in the public domain.
