class TEST_STR63
create make
feature {NONE} make
	local maker: STR63
do
	create maker.encode("")	check maker.value = 0x4fffffffffffffff end
	maker.encode("a"); check not maker.is_lossy; maker.as_string.same_string("a") end
	maker.encode("aj"); check not maker.is_lossy; maker.as_string.same_string("aj") end
	maker.encode("_.~"); check not maker.is_lossy; maker.as_string.same_string("_.~") end
	maker.encode("a~-"); check not maker.is_lossy; maker.as_string.same_string("a~-") end
	maker.encode("The"); check not maker.is_lossy; maker.as_string.same_string("The") end
	maker.encode("fast"); check not maker.is_lossy; maker.as_string.same_string("fast") end
	maker.encode("brown"); check not maker.is_lossy; maker.as_string.same_string("brown") end
	maker.encode(".FOX."); check not maker.is_lossy; maker.as_string.same_string(".FOX.") end
	maker.encode("jumps0"); check not maker.is_lossy; maker.as_string.same_string("jumps0") end
	maker.encode("overthe"); check not maker.is_lossy; maker.as_string.same_string("overthe") end
	maker.encode("lazy.dog"); check not maker.is_lossy; maker.as_string.same_string("lazy.dog") end
	maker.encode("8420soooo"); check not maker.is_lossy; maker.as_string.same_string("8420soooo") end
	maker.encode("quickquick"); check not maker.is_lossy; maker.as_string.same_string("quickquick") end
	maker.encode("The"); check not maker.is_lossy; maker.as_string.same_string("The") end
	maker.encode("quick"); check not maker.is_lossy; maker.as_string.same_string("quick") end
	maker.encode("brown"); check not maker.is_lossy; maker.as_string.same_string("brown") end
	maker.encode("fox"); check not maker.is_lossy; maker.as_string.same_string("fox") end
	maker.encode("jumps"); check not maker.is_lossy; maker.as_string.same_string("jumps") end
	maker.encode("over"); check not maker.is_lossy; maker.as_string.same_string("over") end
	maker.encode("the"); check not maker.is_lossy; maker.as_string.same_string("the") end
	maker.encode("lazy"); check not maker.is_lossy; maker.as_string.same_string("lazy") end
	maker.encode("dog."); check not maker.is_lossy; maker.as_string.same_string("dog.") end
	maker.encode("quick.quick"); check not maker.is_lossy; maker.as_string.same_string("quick.quick") end
	maker.encode("quuckQUICK_"); check not maker.is_lossy; maker.as_string.same_string("quuckQUICK_") end
	maker.encode("QUICK.quick"); check not maker.is_lossy; maker.as_string.same_string("QUICK.quick") end
	maker.encode("quick_QUICK"); check not maker.is_lossy; maker.as_string.same_string("quick_QUICK") end
	maker.encode("Yeahlalland"); check not maker.is_lossy; maker.as_string.same_string("Yeahlalland") end
	maker.encode("yEAHLLALAND"); check not maker.is_lossy; maker.as_string.same_string("yEAHLLALAND") end
	maker.encode("evens..2468"); check not maker.is_lossy; maker.as_string.same_string("evens..2468") end
	maker.encode("13579__ODDS"); check not maker.is_lossy; maker.as_string.same_string("13579__ODDS") end
	maker.encode("yeahlalaland"); check not maker.is_lossy; maker.as_string.same_string("yeahlalaland") end
	maker.encode("YEAHLALALAND"); check not maker.is_lossy; maker.as_string.same_string("YEAHLALALAND") end
	maker.encode("MSFT"); check not maker.is_lossy; maker.as_string.same_string("MSFT") end
	maker.encode("google.com"); check not maker.is_lossy; maker.as_string.same_string("google.com") end
	maker.encode("~jack"); check not maker.is_lossy; maker.as_string.same_string("~jack") end
	maker.encode("smallCamel"); check not maker.is_lossy; maker.as_string.same_string("smallCamel") end
	maker.encode("MAGIC_NUMBER"); check not maker.is_lossy; maker.as_string.same_string("MAGIC_NUMBER") end
	maker.encode("20190202"); check not maker.is_lossy; maker.as_string.same_string("20190202") end
	maker.encode("Kennedy"); check not maker.is_lossy; maker.as_string.same_string("Kennedy") end
	maker.encode("username12"); check not maker.is_lossy; maker.as_string.same_string("username12") end
	maker.encode("accomplishment"); check maker.is_lossy; maker.as_string.same_string("accomplishme") end
	maker.encode("Καλημέρα World"); check maker.is_lossy; maker.as_string.same_string("World") end
	maker.encode("with, it!"); check maker.is_lossy; maker.as_string.same_string("withit") end
	end
end
