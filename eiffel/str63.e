note
	description: "Encoder/decoder for STR63 tokens"
	machine_generated: "This code is cross-compiled from the Rust STR63 implementation"

class STR63
create
	encode, from_value
feature {ANY}
	value: INTEGER_64
			-- Token encoded using the STR63 method
	is_lossy: BOOLEAN
			-- Has last encoding skipped some characters from input string?
feature {ANY}
	is_value (v: INTEGER_64): BOOLEAN
			-- Valid encoded value?
		do Result := v > 0x4000000000000000 end
	from_value (v: INTEGER_64)
			-- Initialize from already encoded value
		require valid_encoding: is_value (v)
		do value := v end
	encode (s: attached READABLE_STRING_GENERAL)
			-- Encode string to STR63 value
		local MAX_UNUSED: INTEGER_64
			CAPSLOCK: INTEGER_64
			NUM_LOCK: INTEGER_64
			used: INTEGER
			unused: INTEGER
			lossy: BOOLEAN
			num: BOOLEAN
			upper: BOOLEAN
			enc: INTEGER_64
			next: INTEGER
			code: NATURAL_32
			enc1: NATURAL_32
		do MAX_UNUSED := 64
			CAPSLOCK := 31
			NUM_LOCK := 30
			used := 0
			unused := 0
			lossy := False
			num := False
			upper := False
			enc := 4
			next := 1
			from until not ((used < 12 and then unused < MAX_UNUSED and then (next <= s.count))) loop
				code := s.code (next)
				next := next + 1
				if (48 <= code and code <= 57) and (num = False) and (used = 0) then
					enc := enc | 1
					num := True
				elseif ((97 <= code and code <= 106) and (num = True)) or else ((65 <= code and code <= 74) and (num = True)) or else ((48 <= code and code <= 57) and (num = False) and (1 <= used and used <= 12)) then
					enc := ((enc |<< 5)) | NUM_LOCK
					used := used + 1
					num := not num
				elseif ((48 <= code and code <= 57) and (num = True)) or else ((code = 46) and (num = True)) then
					check num end
				else num := False
				end
				if (65 <= code and code <= 90) and (upper = False) and (used = 0) then
					enc := enc | 2
					upper := True
				elseif ((97 <= code and code <= 122) and (upper = True)) or else ((65 <= code and code <= 90) and (upper = False) and (1 <= used and used <= 12)) then
					enc := ((enc |<< 5)) | CAPSLOCK
					used := used + 1
					upper := not upper
				else
				end
				if 97 <= code and code <= 122 then enc1 := (code & 31)
				elseif 65 <= code and code <= 90 then enc1 := (code & 31)
				elseif 48 <= code and code <= 57 then enc1 := code + 1 - 48
				elseif code = 45 then enc1 := 27
				elseif code = 46 then enc1 := 0
				elseif code = 95 then enc1 := 28
				elseif code = 126 then enc1 := 29
				else enc1 := 32 end
				if enc1 = 32
				then unused := unused + 1
					lossy := True
				else check used < 12 end
					check enc1 < 32 end
					enc := ((enc |<< 5)) | enc1.as_integer_64
					used := used + 1 end
			end
			lossy := lossy or else (next <= s.count)
			from until not (used < 12) loop
				enc := ((enc |<< 5)) | CAPSLOCK
				used := used + 1
			end
			value := enc
			is_lossy := lossy
		end

	as_string: STRING_8
			-- Regular string from STR63-encoded value.
		local enc: INTEGER_64
			upper: BOOLEAN
			numbr: BOOLEAN
			i: INTEGER
			t: INTEGER
			char5: NATURAL_8
		do create Result.make (12)
			enc := value check enc >= 4611686018427387904 end

			upper := (((enc |>> 61) & 1)) /= 0
			numbr := (((enc |>> 60) & 1)) /= 0
			i := 0
			t := 55
			from until not (t >= 0) loop
				char5 := (((enc |>> t) & 31)).as_natural_8
				if (1 <= char5 and char5 <= 26) and (upper = False) and (numbr = False) then
					Result.append_code (char5 | 96)
					i := i + 1
				elseif (1 <= char5 and char5 <= 26) and (upper = True) and (numbr = False) then
					Result.append_code (char5 | 64)
					i := i + 1
				elseif (1 <= char5 and char5 <= 10) and (numbr = True) then
					Result.append_code (char5 + 48 - 1)
					i := i + 1
				elseif (11 <= char5 and char5 <= 26) and (upper = False) and (numbr = True) then
					Result.append_code (char5 | 96)
					i := i + 1
					numbr := False
				elseif (11 <= char5 and char5 <= 26) and (upper = True) and (numbr = True) then
					Result.append_code (char5 | 64)
					i := i + 1
					numbr := False
				elseif (char5 = 0) then
					Result.append_code (46)
					i := i + 1
				elseif (char5 = 27) then
					Result.append_code (45)
					i := i + 1
					numbr := False
				elseif (char5 = 28) then
					Result.append_code (95)
					i := i + 1
					numbr := False
				elseif (char5 = 29) then
					Result.append_code (126)
					i := i + 1
					numbr := False
				elseif (char5 = 31) then
					upper := not upper
					numbr := False
				elseif (char5 = 30) then
					numbr := not numbr
				else check False end
				end
				t := t - 5
			end
		end

invariant valid_encoding: is_value (value)

end
